import 'package:flutter/material.dart';
import 'package:uts_ecommerce_adwpcell/model/userViewModel.dart';


class KeranjangPage extends StatefulWidget {
  @override
  _KeranjangPageState createState() => _KeranjangPageState();
}

class _KeranjangPageState extends State<KeranjangPage> {

List dataTransaksi = new List();

  void getTransaksi() {
    UserViewModel().getTransaksi().then((value) {
      setState(() {
        dataTransaksi = value;
      });
    });
  }

  @override
  void initState() {
    getTransaksi();
    super.initState();
  }

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final price = "20000";

  var bloc;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text("Keranjang"),
      ),
      body: dataTransaksi == null ? Center(child: CircularProgressIndicator(),) : ListView.builder(itemCount: dataTransaksi.length,itemBuilder: (context, i){
        return Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
            alignment: Alignment.centerRight,
              color: Colors.black.withOpacity(0.05),
              padding: EdgeInsets.all(20),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(

                        "Tanggal : " + dataTransaksi[i]['tgl_jual'],
                        style: TextStyle(
                            fontSize: 14,
                            color: Theme.of(context).primaryColor,
                            fontWeight: FontWeight.w300),
                            textAlign: TextAlign.left,
                      ),
                  Text(
                        "Nama : " + dataTransaksi[i]['nama_pembeli'],
                        style: TextStyle(
                            fontSize: 14,
                            color: Theme.of(context).primaryColor,
                            fontWeight: FontWeight.w300),
                            textAlign: TextAlign.left,
                      ),
                  Text(
                        "Alamat dan Nomor Hp: " + dataTransaksi[i]['alamat'],
                        style: TextStyle(
                            fontSize: 14,
                            color: Theme.of(context).primaryColor,
                            fontWeight: FontWeight.w300),
                            textAlign: TextAlign.left,
                      ),
                  Text(
                        "Jumlah : " + dataTransaksi[i]['jumlah'],
                        style: TextStyle(
                            fontSize: 14,
                            color: Theme.of(context).primaryColor,
                            fontWeight: FontWeight.w300),
                            textAlign: TextAlign.left,
                      ),
                  Text(
                        "Metode Pembayaran : " + dataTransaksi[i]['metode_pembayaran'],
                        style: TextStyle(
                            fontSize: 14,
                            color: Theme.of(context).primaryColor,
                            fontWeight: FontWeight.w300),
                            textAlign: TextAlign.left,
                      ),
                  Text(
                        "Nama Barang: " + dataTransaksi[i]['nama'],
                        style: TextStyle(
                            fontSize: 14,
                            color: Theme.of(context).primaryColor,
                            fontWeight: FontWeight.w300),
                            textAlign: TextAlign.left,
                      ),
                  Text(
                        "Harga : " + dataTransaksi[i]['harga'],
                        style: TextStyle(
                            fontSize: 14,
                            color: Theme.of(context).primaryColor,
                            fontWeight: FontWeight.w300),
                            textAlign: TextAlign.left,
                      ),
                      
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      
                      
                    ],
                  ),
                ],
              ),
            )
          ],
        ),
      );
      })
    );
  }

  Widget list() {
    return ListView(
        );
  }
}
