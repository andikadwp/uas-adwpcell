import 'package:flutter/material.dart';

import 'package:flutter/cupertino.dart';
import 'package:uts_ecommerce_adwpcell/model/userPostModel.dart';
import 'package:uts_ecommerce_adwpcell/model/userViewModel.dart';
import 'package:uts_ecommerce_adwpcell/page/keranjang_page.dart';

class TambahBarang extends StatefulWidget {
final int id;

TambahBarang({@required this.id});

  @override
  TambahBarangState createState() => TambahBarangState();
}

//class controller
class TambahBarangState extends State<TambahBarang> with TickerProviderStateMixin {
  
  // TambahBarangState(this.contact);

  TextEditingController namaController = TextEditingController();
  TextEditingController alamatController = TextEditingController();
  TextEditingController jumlahController = TextEditingController();
  TextEditingController metodeController = TextEditingController();
 String dropdownvalue = 'BRI';
  var items =  ['BRI','BCA','Mandiri','BSI','Alfamart','Indomart'];
  @override
  Widget build(BuildContext context) {
    //kondisi

    //rubah
    return SafeArea(
        child: Scaffold(
            appBar: AppBar(
              title: new Center(
                  child: new Text("ADWP CELL", textAlign: TextAlign.center)),
              elevation: 0.0,
              backgroundColor: Colors.cyan,
              actions: <Widget>[
                Stack(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.all(10.0),
                      child: InkResponse(
                        onTap: () {
                        },
                        child: Tab(
                          icon: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Icon(Icons.shopping_cart),
                              Container(
                                alignment: Alignment.center,
                                width: 18,
                                height: 18,
                                decoration: BoxDecoration(
                                    color: Colors.red,
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(5))),
                                child: Text(
                                  "0",
                                  style: TextStyle(color: Colors.white),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    )
                  ],
                )
              ],
            ),
            body: Padding(
              padding: EdgeInsets.only(top: 15.0, left: 10.0, right: 10.0),
              child: ListView(
                children: <Widget>[
                  // username
                  Padding(
                    padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
                    child: TextField(
                      controller: namaController,
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                          labelText: 'Nama Pembeli',
                          focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.black87)),
                          border: UnderlineInputBorder()),
                      onChanged: (value) {
                        //
                      },
                    ),
                  ),
                  // nama
                  Padding(
                    padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
                    child: TextField(
                      controller: alamatController,
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                          labelText: 'Alamat dan No Hp',
                          focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.black87)),
                          border: UnderlineInputBorder()),
                      onChanged: (value) {
                        //
                      },
                    ),
                  ),

                  // deskripsi
                  Padding(
                    padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
                    child: TextField(
                      controller: jumlahController,
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                          labelText: 'Jumlah',
                          focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.black87)),
                          border: UnderlineInputBorder()),
                      onChanged: (value) {
                        //
                      },
                    ),
                  ),

                 Padding(padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
                 child: Text("Metode Pembayaran")),
                  Padding(
                    padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
                    child: DropdownButton(
                     
                      value: dropdownvalue,
                  icon: Icon(Icons.keyboard_arrow_down),
                  items:items.map((String items) {
                       return DropdownMenuItem(
                           value: items,
                           child: Text(items)
                       );
                  }
                  ).toList(),
                onChanged: (String newValue){
                  setState(() {
                    dropdownvalue = newValue;
                  });
                },)
                  ),
                   
                  new InkWell(
                    onTap: () {
                      UserpostModel commRequest = UserpostModel();
    commRequest.nama_pembeli = namaController.text;
    commRequest.alamat = alamatController.text;
    commRequest.jumlah = jumlahController.text;
    commRequest.metode_pembayaran = dropdownvalue;
    commRequest.id_barang = widget.id;
    

    UserViewModel()
        .postUser(userpostModelToJson(commRequest))
        .then((value) => print('success'));
print(namaController.text+ widget.id.toString() + alamatController.text + "ini jumlah :" + jumlahController.text + dropdownvalue);
        Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context)=> KeranjangPage()));

        

                    },
                    child: Container(
                      width: 200,
                      padding: EdgeInsets.symmetric(vertical: 15),
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(5)),
                          boxShadow: <BoxShadow>[
                            BoxShadow(
                                color: Colors.grey.shade200,
                                offset: Offset(2, 4),
                                blurRadius: 5,
                                spreadRadius: 2)
                          ],
                          gradient: LinearGradient(
                              begin: Alignment.centerLeft,
                              end: Alignment.centerRight,
                              colors: [Colors.blue, Colors.lightBlue[100]])),
                      child: Text(
                        'Tambah Produk',
                        style: TextStyle(fontSize: 20, color: Colors.black),
                      ),
                    ),
                  )
                ],
              ),
            )));
  }
}
