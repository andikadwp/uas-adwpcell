import 'package:flutter/material.dart';
import 'package:uts_ecommerce_adwpcell/model/userViewModel.dart';
import 'package:uts_ecommerce_adwpcell/page/Tambah_page.dart';
import 'package:uts_ecommerce_adwpcell/page/contact_page.dart';
import 'package:uts_ecommerce_adwpcell/page/keranjang_page.dart';
import 'package:uts_ecommerce_adwpcell/page/login_page.dart';
import 'package:uts_ecommerce_adwpcell/page/profile_page.dart';

class HomePage extends StatefulWidget {
  

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List dataBarang = new List();

  void getDataBarang() {
    UserViewModel().getBarang().then((value) {
      setState(() {
        dataBarang = value;
      });
    });
  }

  @override
  void initState() {
    getDataBarang();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.lightBlue[50],
        appBar: AppBar(
          title: new Center(
              child: new Text("ADWP CELL", textAlign: TextAlign.center)),
          elevation: 0.0,
          backgroundColor: Colors.cyan,
          actions: <Widget>[
            Stack(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(10.0),
                  child: InkResponse(
                     
                    child: Tab(
                      
                       icon: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Icon(Icons.shopping_cart),
                          
                          Container(
                            alignment: Alignment.center,
                            width: 18,
                            height: 18,
                            decoration: BoxDecoration(
                                color: Colors.red,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(5))),
                            child: Text(
                              "0",
                              style: TextStyle(color: Colors.white),
                          
                            ),
                          )
                        ],
                        
                      ),
                    ),
                  ),
                )
              ],
            )
          ],
        ),
        body: Stack(children: <Widget>[
          
          Container(
            decoration: new BoxDecoration(
                gradient: LinearGradient(colors: [Colors.cyan, Colors.blue]),
                borderRadius: new BorderRadius.only(
                    bottomLeft: const Radius.circular(0.0),
                    bottomRight: const Radius.circular(0.0))),
            height: 90.0,
            child: Row(
              mainAxisAlignment:MainAxisAlignment.spaceEvenly,
              children: [Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        CircleAvatar(
                          backgroundColor: Colors.green,
                          radius: 25,
                          child: CircleAvatar(
                              backgroundColor: Colors.white,
                              radius: 25,
                              child: Icon(
                                Icons.phone_android,
                                size: 25,
                              )), //CircleAvatar
                        ),
                        Text(
                          'Samsung',
                          style: TextStyle(color: Colors.white, fontSize: 20),
                        )
                      ],
                    ),

                    Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        CircleAvatar(
                          backgroundColor: Colors.green,
                          radius: 25,
                          child: CircleAvatar(
                              backgroundColor: Colors.white,
                              radius: 25,
                              child: Icon(
                                Icons.phone_android,
                                size: 25,
                              )), //CircleAvatar
                        ),
                        Text(
                          'Oppo',
                          style: TextStyle(color: Colors.white, fontSize: 20),
                        )
                      ],
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        CircleAvatar(
                          backgroundColor: Colors.green,
                          radius: 25,
                          child: CircleAvatar(
                              backgroundColor: Colors.white,
                              radius: 25,
                              child: Icon(
                                Icons.phone_android,
                                size: 25,
                              )), //CircleAvatar
                        ),
                        Text(
                          'Iphone',
                          style: TextStyle(color: Colors.white, fontSize: 20),
                        )
                      ],
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        CircleAvatar(
                          backgroundColor: Colors.green,
                          radius: 25,
                          child: CircleAvatar(
                              backgroundColor: Colors.white,
                              radius: 25,
                              child: Icon(
                                Icons.phone_android,
                                size: 25,
                              )), //CircleAvatar
                        ),
                        Text(
                          'Xiaomi',
                          style: TextStyle(color: Colors.white, fontSize: 20),
                        )
                      ],
                    ), ],
            ),
          ),
          
          
          Container(
            margin: EdgeInsets.only(top: 90),
            child: GridView.builder(gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                maxCrossAxisExtent: 400,
                childAspectRatio: 5 / 6,
                crossAxisSpacing: 5,
                mainAxisSpacing: 20), 
                itemCount: dataBarang.length,
                itemBuilder: (context,i)
                {
                  return  Container(
                    color: Colors.white,
                    margin: EdgeInsets.all(25.0),
                    child: Column(
                      children: [
                        
                        Image.network(
                          dataBarang[i]['gambar'],
                          cacheHeight: 100,
                          cacheWidth: 100,
                        ),
                        Text(dataBarang[i]['nama']),
                        Text(
                          "Rp. " + dataBarang[i]['harga'],
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        RaisedButton(shape: RoundedRectangleBorder (borderRadius: BorderRadius.circular(15)),
                         onPressed: () {
                         Navigator.push(context, 
                      MaterialPageRoute(builder: (context) => TambahBarang(id : int.parse(dataBarang[i]['id_barang']))
                        )
                      );
                    },
                        
                         padding: EdgeInsets.all(5),
                         color: Colors.cyan[400],
                         child: Text('Beli', style: TextStyle(color: Colors.white)),
                        )
                      ],
                    
                    ));
                },
              
            ),
          ),
        ]),
        drawer: Drawer(
          child: ListView(
            padding: EdgeInsets.zero,
            children: <Widget>[
              new Container(
                color: Colors.transparent,
                child: UserAccountsDrawerHeader(
                  accountName: Text("Adwp"),
                  accountEmail: Text("Adwp@gmail.com"),
                ),
              ),
              ListTile(
                title: Text('My Orders'),
                leading: Icon(Icons.shopping_basket),
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => KeranjangPage()));
                },
              ),
              ListTile(
                title: Text('Tambah Barang'),
                leading: Icon(Icons.add),
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => TambahBarang()));
                 
                },
              ),
              ListTile(
                title: Text('About'),
                leading: Icon(Icons.account_circle_outlined),
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => ContactPage()));
                 
                },
              ),
              ListTile(
                title: Text('Profile'),
                leading: Icon(Icons.account_box_sharp),
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => Profile()));
              
                },
              ),
              Divider(),
              ListTile(
                title: Text('Logout'),
                leading: Icon(Icons.exit_to_app),
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => LoginPage()));
                },
              )
            ],
          ),
        ));
  }
}


