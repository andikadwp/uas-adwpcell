import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:uts_ecommerce_adwpcell/model/userPostModel.dart';


class UserViewModel {
  Future getTransaksi() async {
    try {
      http.Response hasil = await http.get(
          Uri.encodeFull("https://andikadwp.000webhostapp.com/read_transaksi.php"),
          headers: {"Accept": "application/json"});
      if (hasil.statusCode == 200) {
        print("data category success");
        final data = userModelFromJson(hasil.body);
        print(data);
        return data;
      } else {
        print("error status " + hasil.statusCode.toString());
        return null;
      }
    } catch (e) {
      print("error catch $e");
      return null;
    }
  }
  
Future getBarang() async {
    try {
      http.Response hasil = await http.get(
          Uri.encodeFull("https://andikadwp.000webhostapp.com/read_barang.php"),
          headers: {"Accept": "application/json"});
      if (hasil.statusCode == 200) {
        print("data category success");
        final data = userModelFromJson(hasil.body);
        print(data);
        return data;
      } else {
        print("error status " + hasil.statusCode.toString());
        return null;
      }
    } catch (e) {
      print("error catch $e");
      return null;
    }
  }
  Future postUser(body) async {
    try {
      http.Response hasil = await http.post(
          Uri.encodeFull("https://andikadwp.000webhostapp.com/create_pemesanan.php"),
          body: body,
          headers: {
            "Accept": "application/json",
          });
      if (hasil.statusCode == 200) {
        print("status 200");
        // final data = json.decode(hasil.body);
        return true;
      } else {
        print("error status " + hasil.statusCode.toString());
        return null;
      }
    } catch (e) {
      print("error catchnya $e");
      return null;
    }
  }
}
